# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 18:02:25 2019

@author: lpaone
"""
import math

# Defines points or free vector in 2D space
class Point2d(object):
    
    def __init__(self, x = 0.0, y = 0.0):
        self.x = x
        self.y = y
        
    def __str__(self):
        return "Point2D: x = %.3e, y = %.3e" % (self.x, self.y)

    def __add__(self, other):
        return Point2d(self.x+other.x,self.y+other.y)
        
    def __sub__(self, other):
        return Point2d(self.x-other.x,self.y-other.y)

    def __mul__(self, other):
        return Point2d(self.x*other,self.y*other)
        
    def __rmul__(self, other):
        return Point2d(self.x*other,self.y*other)
        
    def translate(self, dx = 0., dy = 0.):
        self.x = self.x + dx
        self.y = self.y + dy
        
class Node2d(Point2d):
    
    def __init__(self, x = 0.0, y = 0.0, idn = -1):
        Point2d.__init__(self, x, y)
        self.idn = idn

    def __str__(self):
        return "Node2D: idn = %.0f, x = %.3e, y = %.3e" % (self.idn, self.x, self.y)

# Defines points or free vector in 3D space
class Point3d(object):
    
    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = x
        self.y = y
        self.z = z
        
    def __str__(self):
        return "Point3D: x = %.3e, y = %.3e, z = %.3e" % (self.x, self.y, self.z)
        
    def __add__(self, other):
        return Point3d(self.x+other.x,self.y+other.y,self.z+other.z)

    def __sub__(self, other):
        return Point3d(self.x-other.x,self.y-other.y,self.z-other.z)

    def tranlate(self, dx = 0., dy = 0., dz = 0.):
        self.x = self.x + dx
        self.y = self.y + dy 
        self.z = self.z + dz        

# Defines points or free vector in 3D space
class Vector3d(object):
    
    def __init__(self, *args):
        if len(args) == 3:
            for a in args:
                if isinstance(a,float) == False:
                    raise Exception("With len args == 3 only float type !!!")

            self.vx = args[0]
            self.vy = args[1]
            self.vz = args[2]

        elif len(args) == 2:
            for a in args:
                if isinstance(a,Point3d) == False:
                    raise Exception("With len args == 2 only Point3d type !!!")

            self.vx = args[1].x-args[0].x
            self.vy = args[1].y-args[0].y
            self.vz = args[1].z-args[0].z
            
        else:
            raise Exception("Wrong args !!!")
        
    def __str__(self):
        return "Vector3D: vx = %.3e, vy = %.3e, vz = %.3e" % (self.vx, self.vy, self.vz)
        
    def __add__(self, other):
        return Vector3d(self.vx+other.vx,self.vy+other.vy,self.vz+other.vz)
        
    def __sub__(self, other):
        return Vector3d(self.vx-other.vx,self.vy-other.vy,self.z-other.vz)
        
    # def __mul__(self, scale):
    #    return Vector3d(self.vx*scale,self.vy*scale,self.z*scale)
        
    def __rmul__(self, scale):
        return Vector3d(self.vx*scale,self.vy*scale,self.vz*scale)        
        
    #       _    _    _            _         _         _
    #     | x    y    z  |   y1*z2*x + z1*x2*y + x1*y2*z + 
    #  det| x1   y1   z1 | =       _         _         _  
    #     | x2   y2   z2 |  -y2*z1*x - x1*z2*y - x2*y1*z        
    #
    def cross(self, other):
        x1 = self.vx
        y1 = self.vy
        z1 = self.vz
        x2 = other.vx
        y2 = other.vy
        z2 = other.vz
        x3 = y1*z2 - y2*z1
        y3 = z1*x2 - x1*z2
        z3 = x1*y2 - x2*y1
        return Vector3d(x3,y3,z3)
        
    def norm(self):
        return math.sqrt(math.pow(self.vx,2) + math.pow(self.vy,2) + math.pow(self.vz,2))
        
    def normalize(self):
        n = self.norm()
        if n!=0:
            self.vx = self.vx/n
            self.vy = self.vy/n
            self.vz = self.vz/n
        else:
            raise Exception("Vector 3D null !!!")
        return self
        
class Node3d(Point3d):
    
    def __init__(self, x = 0.0, y = 0.0, z = 0.0, idn = -1):
        Point3d.__init__(self, x, y, z)
        self.idn = idn

    def __str__(self):
        return "Node3D: idn = %.0f, x = %.3e, y = %.3e, z = %.3e" % (self.idn, self.x, self.y, self.z)
        
    def __add__(self, other):
        return Node3d(self.x+other.x,self.y+other.y,self.z+other.z)
        
    def __sub__(self, other):
        return Node3d(self.x-other.x,self.y-other.y,self.z-other.z)
        
class Edge2d(object):
    
    def __init__(self, *args):
        if len(args) == 2:
            if isinstance(args[0],Node2d) & isinstance(args[1],Node2d) == True:
                self.__node_i = args[0];
                self.__node_j = args[1];                
            else:
                raise Exception("Args must be [Node2D] !!!")
        else:
            raise Exception("Only two argument !!!")

    def nodeI(self):
        return self.__node_i

    def nodeJ(self):
        return self.__node_j
        
    def __str__(self):
        dispstr = "Node2D i: idn = %.0f, x = %.3e, y = %.3e \n" % (self.__node_i.idn, self.__node_i.x, self.__node_i.y) 
        dispstr = dispstr + "Node2D j: idn = %.0f, x = %.3e, y = %.3e \n" % (self.__node_j.idn, self.__node_j.x, self.__node_j.y) 
        return dispstr
        
class Edge3d(object):
    
    def __init__(self, *args):
        if len(args) == 2:
            if isinstance(args[0],Node3d) & isinstance(args[1],Node3d) == True:
                self.__node_i = args[0]
                self.__node_j = args[1]                
            else:
                raise Exception("Args must be [Node3D] !!!")
        elif len(args) == 0:
            self.__node_i = Node3d()
            self.__node_j = Node3d()         
        elif len(args) == 8:
            for i in range(2):
                if isinstance(args[0+4*i],float) == False:
                    raise Exception("Arg n. %e must be float !!!"%(0+i))
                if isinstance(args[1+4*i],float) == False:
                    raise Exception("Arg n. %e must be float !!!"%(1+i))                    
                if isinstance(args[2+4*i],float) == False:
                    raise Exception("Arg n. %e must be float !!!"%(2+i))                    
                if isinstance(args[3+4*i],int) == False:                    
                    raise Exception("Arg n. %e must be int !!!"%(3+i))
            self.__node_i = Node3d(args[0],args[1],args[2],args[3])
            self.__node_j = Node3d(args[4],args[5],args[6],args[7])            
        else:
            raise Exception("Wrong arguments !!!")

    def nodeI(self):
        return self.__node_i

    def nodeJ(self):
        return self.__node_j
     
    def setNodeI(self, *args):
        if len(args) == 1:
            if isinstance(args[0],Node3d) == True:
                self.__node_i = args[0];   
            else:
                raise Exception("Arg must be [Node3D] !!!")
        else:
            raise Exception("Only one argument !!!")

    def setNodeJ(self, *args):
        if len(args) == 1:
            if isinstance(args[0],Node3d) == True:
                self.__node_j = args[0];   
            else:
                raise Exception("Arg must be [Node3D] !!!")
        else:
            raise Exception("Only one argument !!!")       
        
    def __str__(self):
        dispstr = "Node3D i: idn = %.0f, x = %.3e, y = %.3e, z = %.3e \n" % (self.__node_i.idn, self.__node_i.x, self.__node_i.y, self.__node_i.z) 
        dispstr = dispstr + "Node3D j: idn = %.0f, x = %.3e, y = %.3e, z = %.3e \n" % (self.__node_j.idn, self.__node_j.x, self.__node_j.y, self.__node_j.z) 
        return dispstr
        
    def lenght(self):
        return math.sqrt(math.pow(self.__node_j.x-self.__node_i.x,2)+
                         math.pow(self.__node_j.y-self.__node_i.y,2)+
                         math.pow(self.__node_j.z-self.__node_i.z,2))
        
class Polyline2d(object):
    """
    List of Nodes2D
    """    
    def __init__(self, *args):
        if len(args) == 0:
            raise Exception("Only one argument !!!")
        elif len(args) == 1:
            lst = args[0]
            if isinstance(lst,list) == True:
                for i in lst:
                    if isinstance(i,Node2d) == False:
                        raise Exception("List must be [Node2D] formed !!!")
                self.__node = lst;
            else:
                raise Exception("First arg must be type List !!!")
        elif len(args) > 1:
            raise Exception("Only one argument !!!")
    
    def __str__(self):
        dispstr = "Polyline2d: size is %.0f \n" % len(self.__node)
        for n in self.__node:
            dispstr = dispstr + "Node2D: idn = %.0f, x = %.3e, y = %.3e \n" % (n.idn, n.x, n.y) 
        return dispstr
   
    def getNodes(self):
        return self.__node
        
    def setClosed(self):
        self.__node.append(self.__node[0])
        return
        
    def isClosed(self):
        sz = len(self.__node)
        if self.__node[0] == self.__node[sz-1]:
            return True
        else:
            return False
            
    def translate(self, pStart = Point2d(), pEnd = Point2d()):
        v = pEnd - pStart
        print(type(v))
        for node in self.__node:
            node.translate(v.x,v.y)
        return
        
    def vertexNb(self):
        return len(self.__node)     
        
    def vertexAt(self,i):
        return len(self.__node[i])         
    
class Polyline3d(object):
    """
    List of Nodes3D
    """
    def __init__(self, *args):
        if len(args) == 0:
            raise Exception("Only one argument !!!")
        elif len(args) == 1:
            lst = args[0]
            if isinstance(lst,list) == True:
                for i in lst:
                    if isinstance(i,Node3d) == False:
                        raise Exception("List must be [Node3D] formed !!!")
                self.__node = lst;
            else:
                raise Exception("First arg must be type List !!!")
        elif len(args) > 1:
            raise Exception("Only one argument !!!")
    
    def __str__(self):
        dispstr = "Polyline3d: size is %.0f \n" % len(self.__node)
        for n in self.__node:
            dispstr = dispstr + "Node3D: idn = %.0f, x = %.3e, y = %.3e, z = %.3e\n" % (n.idn, n.x, n.y, n.z) 
        return dispstr
           
    def setClosed(self):
        self.__node.append(self.__node[0])
        return
        
    def isClosed(self):
        sz = len(self.__node)
        if self.__node[0] == self.__node[sz-1]:
            return True
        else:
            return False
            
    def vertexNb(self):
        return len(self.__node)  
    
    def vertexAt(self,i):
        return len(self.__node[i])    
        
class Frame(object):
    
    """
    n1=Node3d(int,float,)
    e=Edge3d()
    f = Frame(e=Edge3d)
    """
    def __init__(self, *args):
        if len(args) == 0:
            raise Exception("Only one argument !!!")
        elif len(args) == 1:
            if isinstance(args[0],Edge3d) == True:
                self.__axis = args[0]
                self.__id = -1
                self.__reference = Point3d()
                self.__shape = None
            else:
                raise Exception("Arg must be [Edge3D] !!!")
        elif len(args) == 8:
            for i in range(2):
                if isinstance(args[0+4*i],float) == False:
                    raise Exception("Arg n. %e must be float !!!"%(0+i))
                if isinstance(args[1+4*i],float) == False:
                    raise Exception("Arg n. %e must be float !!!"%(1+i))                    
                if isinstance(args[2+4*i],float) == False:
                    raise Exception("Arg n. %e must be float !!!"%(2+i))                    
                if isinstance(args[3+4*i],int) == False:                    
                    raise Exception("Arg n. %e must be int !!!"%(3+i))
            self.__axis = Edge3d(Node3d(args[0],args[1],args[2],args[3]),
                                 Node3d(args[4],args[5],args[6],args[7])) 
            self.__id = -1
            self.__reference = Point3d()
            self.__shape = None                                 
        else:
            raise Exception("Wrong arguments !!!")
    
    def __str__(self):
        dispstr = "Frame Object: \n"
        dispstr = dispstr + "------------- \n"        
        dispstr = dispstr + "id: "  +str(self.__id) + "\n"
        dispstr = dispstr + "axis--> \n"         
        dispstr = dispstr + str(self.__axis)
        dispstr = dispstr + "reference--> \n"                 
        dispstr = dispstr + str(self.__reference)  +"\n"        
        if self.__shape != None:
            dispstr = dispstr + "Shape--> \n"            
            dispstr = dispstr + str(self.__shape)  +"\n"
        return dispstr
    
    # Fix y local axis
    def setReference(self, *args):
        if len(args) == 1:
            if isinstance(args[0],Point3d):
                self.__reference = args[0]
            else:
                raise Exception("Arg must be [Point3D] !!!")  
        elif len(args) == 3:                      
            if isinstance(args[0],float)==False:
                raise Exception("Arg nb 1 must be float !!!")
            if isinstance(args[1],float)==False:
                raise Exception("Arg nb 2 must be float !!!")
            if isinstance(args[2],float)==False:
                raise Exception("Arg nb 1 must be float !!!")
            self.__reference = Point3d(args[0],args[1],args[2])
        else:
            raise Exception("Wrong arguments !!!")
       
    def getXLocalAxis(self):
        return Vector3d(self.__axis.nodeI(),self.__axis.nodeJ()).normalize()
        
    def getZLocalAxis(self):
        xAxis = self.getXLocalAxis()      
        yAxisDir = Vector3d(self.__axis.nodeI(),self.__reference)
        if xAxis.cross(yAxisDir).norm()==0:
            raise Exception("Null cross product: referenze point bad defined !!!")
        return xAxis.cross(yAxisDir).normalize()
        
    def getYLocalAxis(self):
        return self.getZLocalAxis().cross(self.getXLocalAxis())
        
    # Shape
    def setShape(self, *args):
        if len(args) == 1:
            if isinstance(args[0],Shape):
                self.__shape = args[0]
            else:
                raise Exception("Arg must be [Shape] !!!")                        
        else:
            raise Exception("Wrong arguments !!!")
            
    def nodeI(self):
        return self.__axis.nodeI()

    def nodeJ(self):
        return self.__axis.nodeJ()
        
    def lenght(self):
        return self.__axis.lenght()        
        
class Shape(object):
                     
    def __init__(self, ids = -1, typ = -1, originX = 0, originY = 0):      
        self.__ids = ids
        self.__type = typ
        self.__o = Point2d(originX,originY)
    
    def getIds(self):
        return self.__ids

    def getTypes(self):
        return self.__type

    def getDesc(self):
        return "generic"
    
    # def getDesc(self):
        # return self.__desc
        
    def setId(self, ids):
        self.__ids = ids
        
    def setType(self, typ):
        self.__type = typ
        
    # def setDesc(self, desc):
    #   self.__desc = desc

    def setOrigin(self, *args):
        if len(args) == 2:
            if isinstance(args[0],float) and isinstance(args[1],float):
                self.__o.x = args[0]
                self.__o.y = args[1]
            else:
                raise Exception("With 2 args must submit float type !!!")
        elif len(args) == 1:
            if isinstance(args[0],Point2d):
                self.__o.x = args[0].x
                self.__o.y = args[0].y
            else:
                raise Exception("With 2 args must submit float type !!!")                
                
    def getOrigin(self):
        return self.__o
        
    def getArea(self):
        raise Exception("getArea() not implemented for subclass !!!")
        
    def getShapePoint(self, idstr):
        if isinstance(idstr,str):
            if idstr == "O":
                return self.getOrigin()
            elif idstr == "G":
                return self.getOrigin()   
            else:
                raise Exception("Point str undefined !!!")                                
            
    def translate(self, pStart = Point2d(), pEnd = Point2d()):
        self.__o.translate(pEnd.x-pStart.x,pEnd.y-pStart.y)
            
    def vertexNb(self):
        return 0

    def vertexAt(self,i):
        raise Exception("Vertex for [Shape] objects not defined !!!")

    def vertexMaxInY(self):
        if self.vertexNb() ==0:
            raise Exception("Cannot find in 0 vertices number max in Y !!!")
        
        val = []
        vertex = []
        for i in range(self.vertexNb()):
            val.append(self.vertexAt(i).y)
            vertex.append(self.vertexAt(i))
            
        return max(val), vertex[val.index(max(val))]
        
    def vertexMaxInX(self):
        if self.vertexNb() ==0:
            raise Exception("Cannot find in 0 vertices number max in X !!!")
        
        val = []
        vertex = []
        for i in range(self.vertexNb()):
            val.append(self.vertexAt(i).x)
            vertex.append(self.vertexAt(i))
            
        return max(val), vertex[val.index(max(val))]
        
    def vertexMinInY(self):
        if self.vertexNb() ==0:
            raise Exception("Cannot find in 0 vertices number min in Y !!!")
        
        val = []
        vertex = []        
        for i in range(self.vertexNb()):
            val.append(self.vertexAt(i).y)
            vertex.append(self.vertexAt(i))
            
        return min(val), vertex[val.index(min(val))]
        
    def vertexMinInX(self):
        if self.vertexNb() ==0:
            raise Exception("Cannot find in 0 vertices number min in X !!!")
        
        val = []
        vertex = []        
        for i in range(self.vertexNb()):
            val.append(self.vertexAt(i).x)
            vertex.append(self.vertexAt(i))            
        return min(val), vertex[val.index(min(val))] 
        
    def __str__(self):
        dispstr = "Shape Object: \n"
        dispstr = dispstr + "--------------- \n"
        dispstr = dispstr + "  id: "  +str(self.__ids) + "\n"
        dispstr = dispstr + "type: "  +str(self.__type) + " " + self.getDesc() +  "\n"
        return dispstr
            

class ShapePoly(Shape):
    
    def __init__(self, *args):        
        if len(args) == 0:
            raise Exception("Only one or >= 9 argument !!!")
        elif len(args) == 1:
            Shape.__init__(self, -1, 1, 0, 0)
            if isinstance(args[0],Polyline2d) == True:
                self.__polyline = args[0]
            else:
                raise Exception("Arg unknown !!!")
        elif len(args) >= 9:            
            Shape.__init__(self, -1, 1, 0, 0)
            # lst = []
            if (len(args)/3.0 - int(len(args)/3.0)) != 0:
                raise Exception("Args must be multiple of 3 !!!")
            idx = int(len(args)/3.0)
            lst = []
            for i in range(idx):              
                if isinstance(args[0+i*3],float) == False:
                    raise Exception("Args 1*n must be only float !!!")
                if isinstance(args[1+i*3],float) == False:
                    raise Exception("Args 2*n must be only float !!!")   
                if isinstance(args[2+i*3],int) == False:
                    raise Exception("Args 3*n must be only int !!!")
                lst.append(Node2d(args[0+i*3],args[1+i*3],args[2+i*3]))
            self.__polyline = Polyline2d(lst)
        else:
            raise Exception("Wrong arguments !!!")
         
    def getShapePoint(self, idstr):
        if isinstance(idstr,str):
            if idstr == "O":
                return self.getOrigin()
            elif idstr == "G":
                raise Exception("Not defined barycenter for Poliline2D!!!") 
            else:
                raise Exception("Point str undefined !!!")                                
                
    def getDesc(self):
        return("polygonar")
         
    def __str__(self):
        return super(ShapePoly,self).__str__() + "\n" + "Data embedded-->\n" + self.__polyline.__str__() + "\n"
    
    def translate(self, pStart = Point2d(), pEnd = Point2d()):
        super(ShapePoly,self).translate(pStart,pEnd)
        self.__polyline.translate(pStart,pEnd)
        
    def getPolyline2D(self):
        return self.__polyline
        
        
class ShapeRect(Shape):

    def __init__(self, *args):               
        if len(args) == 2:          
            Shape.__init__(self, -1, 101, 0, 0) 
            
            self.__w = args[0]
            self.__h = args[1]
            
        elif len(args) == 0:
            Shape.__init__(self, -1, 101, 0, 0)
            
            self.__w = 0
            self.__h = 0
        
        else:
            raise Exception("Only null o 2 argumets")

    def w(self):
        return self.__w
        
    def h(self):
        return self.__h
        
    def setDimH(self,h):
        self.__h = h
        
    def setDimW(self,w):
        self.__w = w
        
    def getArea(self):
        return self.__w*self.__h
        
    def getDesc(self):
        return("rectangular")

    def getShapePoint(self, idstr):
        if isinstance(idstr,str):
            o = self.getOrigin()            
            TL = Point2d(o.x-self.__w/2.0,o.y+self.__h/2.0)
            TR = Point2d(o.x+self.__w/2.0,o.y+self.__h/2.0)
            BL = Point2d(o.x-self.__w/2.0,o.y-self.__h/2.0)
            BR = Point2d(o.x+self.__w/2.0,o.y-self.__h/2.0)
            
            if idstr == "O":
                return self.getOrigin()
            elif idstr == "G":
                return (TL+BR)*0.5
            elif idstr == "TL":
                return TL
            elif idstr == "TR":
                return TR
            elif idstr == "BL":
                return BL
            elif idstr == "BR":
                return BR
            elif idstr == "MB":
                return (BL+BR)*0.5
            elif idstr == "MT":
                return (TL+TR)*0.5
            else:
                raise Exception("Point str undefined !!!")                                
                         
    def translate(self, pStart = Point2d(), pEnd = Point2d()):
        super(ShapeRect,self).translate(pStart,pEnd)
                
    def vertexNb(self):
        return 4

    def vertexAt(self,i):
        if i==0:
            return self.getShapePoint("BL")
        elif i==1:
            return self.getShapePoint("BR")
        elif i==2:
            return self.getShapePoint("TL")
        elif i==3:
            return self.getShapePoint("TL")
        else:
            raise Exception("i out of the bound !!!") 
            
    def __str__(self):
        dispstr = super(ShapeRect,self).__str__()
        dispstr = dispstr + "Data embedded-->\n"
        dispstr = dispstr + "b = " + str(self.__w) + " h = " + str(self.__h) + "\n"
        dispstr = dispstr + "Origin in " + self.getOrigin().__str__() + "\n"        
        return dispstr
        
class ShapeCircle(Shape):
           
    def __init__(self, *args):               
        if len(args) == 1:          
            Shape.__init__(self, -1, 102, 0, 0)   
            
            self.__r = args[0]
            
        elif len(args) == 0:
            Shape.__init__(self, -1, 102, 0, 0)
            
            self.__r = 0
        else:
            raise Exception("Only null o 1 argumets")

    def getDesc(self):
        return("circle")

    def getShapePoint(self, idstr):
        if isinstance(idstr,str):
            if idstr == "O":
                return self.getOrigin()
            elif idstr == "G":
                return Point2d(self.getOrigin().x,self.getOrigin().y)
            elif idstr == "C": 
                return Point2d(self.getOrigin().x,self.getOrigin().y)
            else:
                raise Exception("Point str undefined !!!")                
    
    def translate(self, pStart = Point2d(), pEnd = Point2d()):
        return super(ShapeCircle,self).translate(pStart,pEnd)
        
    def __str__(self):
        dispstr = super(ShapeCircle,self).__str__()
        dispstr = dispstr + "Data embedded-->\n"
        dispstr = dispstr + "r = " + str(self.__r) + "\n"
        dispstr = dispstr + "Origin in " + self.getOrigin().__str__() + "\n"        
        return dispstr
        
class ShapeArea(Shape):
           
    def __init__(self, *args):               
        if len(args) == 1:          
            Shape.__init__(self, -1, 103, 0, 0)  
            
            self.__area = args[0]
            
        elif len(args) == 0:
            Shape.__init__(self, -1, 103, 0, 0)
            
            self.__area = 0
        else:
            raise Exception("Only null o 1 argumets")

    def setArea(self, a):
        self.__area = a
        
    def getArea(self):
        return self.__area        
        
    def getDesc(self):
        return("area")

    def getShapePoint(self, idstr):
        if isinstance(idstr,str):
            if idstr == "O":
                return self.getOrigin()
            elif idstr == "G":
                return self.getOrigin()
            else:
                raise Exception("Point str undefined !!!")
                
    def __str__(self):
        dispstr = super(ShapeArea,self).__str__()
        dispstr = dispstr + "Data embedded-->\n"
        dispstr = dispstr + "area = " + str(self.__area) + "\n"
        dispstr = dispstr + "Origin in " + self.getOrigin().__str__() + "\n"        
        return dispstr  