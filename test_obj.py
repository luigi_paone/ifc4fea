# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 16:37:47 2019

@author: lpaone
"""

import EXAGeometry as eg


try:
    import EXADisplay as vis
except:
    print("EXADisplay error !!!")

p1 = eg.Point2d(0,0)
p2 = eg.Point2d(1,2./3)
p3 = eg.Point2d(1,2)

print(p1)
print(p2)
print(p2.y)

print("Overriding __mul__ for Point2d(1,2) 2*p3 ---------")
print(2*p3)

print("Overriding __mul__ for Point2d(1,2) p3*3 ---------")
print(p3*3)

n1 = eg.Node2d(101,2.3,4)

print(n1)

n1 = eg.Node2d(0.0,0.0,1)
n2 = eg.Node2d(300.0,0.0,2)
n3 = eg.Node2d(300.0,500.0,3)
n4 = eg.Node2d(0.0,500.0,4)

nodeList = [n1,n2,n3,n4]

try:
    poly1 = eg.Polyline2d([n1,n2],2,3)
except:
    print ("OK Exception poly1 = eg.Polyline2d([n1,n2],2,3)")
    
# Well formed
poly1 = eg.Polyline2d([n1,n2,n3,n4])
print(poly1)

# Without arguments
n1 = eg.Node2d()
n2 = eg.Node2d()
n1.idn = 123
n1.x = 1
n1.y = 2
poly1 = eg.Polyline2d([n1,n2])
print(poly1)

try:
    edge1 = eg.Edge2d(n1)
except Exception as e:
    print ("OK Exception edge1 = eg.Edge2d(n1) --> " + str(e))
    
try:
    edge1 = eg.Edge2d(n1,1)
except Exception as e:
    print ("OK Exception edge1 = eg.Edge2d(n1,1) --> " + str(e))
    
edge1 = eg.Edge2d(n1,n2)    

ni = edge1.nodeI()
nj = edge1.nodeJ()

print(ni)
print(nj)

ni.x = 400
ni.y = 500

print(edge1)

axis = eg.Edge3d(eg.Node3d(1,1,1,1),eg.Node3d(2,3,3,3))
axis1 = eg.Edge3d(eg.Node3d(1,3,3,3),eg.Node3d(2,3,3,0))

frame = eg.Frame(axis)
frame1 = eg.Frame(axis1)
print(frame)

try:
    frame = eg.Frame(1)
except Exception as e:
    print ("OK Exception frame = eg.Frame(1) --> " + str(e))

try:
    frame = eg.Frame(1,1)
except Exception as e:
    print ("OK Exception frame = eg.Frame(1) --> " + str(e))

# Polyline2D testing start
nodesLst = [eg.Node2d(0.,0.,1),eg.Node2d(300.,0.,2),eg.Node2d(300.,500.,3),eg.Node2d(0.,500.,4)]
poly2 = eg.Polyline2d(nodesLst)

print("Polyline 2D...")
print(poly2)
print("Closed ?: %s" %poly2.isClosed())
print("after  setClosed()")
poly2.setClosed()
print("Closed ?: %s" %poly2.isClosed())
print(poly2)

# Polyline3D testing start
nodesLst = [eg.Node3d(1,0.,0.,0.),eg.Node3d(2,300.,0.,0.),eg.Node3d(3,300.,500.,0.),eg.Node3d(4,0.,500.,0.)]
poly3 = eg.Polyline3d(nodesLst)

print("Polyline 3D...")
print(poly3)
print("Closed ?: %s" %poly3.isClosed())
print("after  setClosed()")
poly3.setClosed()
print("Closed ?: %s" %poly3.isClosed())
print(poly3)

# Op for Point3d
p1 = eg.Point3d(0.0,0.0,1.0)
p2 = eg.Point3d(1.0,1.0,0.0)
p3 = p1 + p2
print(p1)
print(p2)
print("p1+p2 is: %s\n"%p3)

# Op for Node3d
n1 = eg.Node3d(0.0,0.0,1.0)
n2 = eg.Node3d(1.0,1.0,0.0)
n3 = n1 + n2
print(n1)
print(n2)
print("n1+n2 is: %s\n"%n3)

# Op for Vector3d
v1 = eg.Vector3d(0.0,0.0,1.0)
v2 = eg.Vector3d(1.0,1.0,0.0)
v3 = v1 + v2
print(v1)
print(v2)
print("v1+v2 is: %s\n"%v3)

# Cross vector for Vector3d
v1 = eg.Vector3d(1.0,0.0,0.0)
v2 = eg.Vector3d(0.0,1.0,0.0)
print(v1)
print(v2)
v3 = v1.cross(v2)
print("v1.cross(v2) is: %s\n"%v3)

# Norm of vector for Vector3d
v1 = eg.Vector3d(5.0,0.0,0.0)
print(v1)
print("v1.norm() is: %d\n"%v1.norm())

# Normalizition of vector for Vector3d
v1.normalize()
print("v1.normalize() is: %s"%v1)

# Vector 3d from two points
p1 = eg.Point3d(1.0,0.0,0.0)
p2 = eg.Point3d(2.0,0.0,0.0)
print(p1)
print(p2)
v1 = eg.Vector3d(p1,p2)
print("v1 from two points: %s\n"%v1)

# Generic Shape Abstraact
# sec = eg.Shape()
# print(sec)

# Polygonar
secPoly = eg.ShapePoly(poly2)
print(secPoly)

# Frame with Shape
frame1.setShape(secPoly)
print(frame1)


# Build a normal frame
print("We want build a typical frame 3D")

n1 = eg.Node3d(1.0,1.0,1.0,1)
n2 = eg.Node3d(6.0,6.0,6.0,2)

axis = eg.Edge3d(n1,n2)

frame_typical = eg.Frame(axis)

frame_typical.setReference(eg.Point3d(1.0,1.0,0.0))
frame_typical.setReference(1.0,1.0,0.0)
print(frame_typical)

print(frame_typical.getXLocalAxis())
print(frame_typical.getYLocalAxis())
print(frame_typical.getZLocalAxis())

# Polygonar
secPoly = eg.ShapePoly(0.,0.,1,300.,0.,2,300.,500.,3,0.,500.,4)

frame_typical.setShape(secPoly)

print(frame_typical)

secPoly.translate(eg.Point2d(150.,250.),eg.Point2d(0.,0.))

print(frame_typical)

axis = eg.Edge3d(1.0,1.0,1.0,1,6.0,6.0,6.0,2)

print(axis)

frame_typical = eg.Frame(100.0,100.0,100.0,1,600.0,600.0,600.0,2)
frame_typical.setReference(100.0,100.0,0.0)
frame_typical.setShape(secPoly)

frame_typical2 = eg.Frame(15.0,25.0,35.0,1,60.0,70.0,90.0,2)
frame_typical2.setReference(15.0,25.0,10.0)
frame_typical2.setShape(secPoly)

print(frame_typical)

# visualizer = vis.Visualizer()
# visualizer.addStruComponent(frame_typical)
# visualizer.addStruComponent(secPoly)
# visualizer.display()


#vis.test_extrusion()
#vis.test_sec()
#vis.test_vector_sec()
#vis.test_axis()
#vis.test_transform()



