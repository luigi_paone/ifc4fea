import OCC.gp
import OCC.Geom

import OCC.Bnd
import OCC.BRepBndLib

import OCC.BRep
import OCC.BRepPrimAPI
import OCC.BRepAlgoAPI
import OCC.BRepBuilderAPI

import OCC.GProp
import OCC.BRepGProp

import OCC.TopoDS
import OCC.TopExp
import OCC.TopAbs

#Only for writing
from OCC.TopoDS import * #TopoDS_Shape, TopoDS_Compound
from OCC.BRepTools import * # breptools_Read
from OCC.BRep import BRep_Builder

import ifcopenshell
import ifcopenshell.geom


# Specify to return pythonOCC shapes from ifcopenshell.geom.create_shape()
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_PYTHON_OPENCASCADE, True)

# Initialize a graphical display window
occ_display = ifcopenshell.geom.utils.initialize_display()

# Open the IFC file using IfcOpenShell
ifc_file = ifcopenshell.open("data/scuola.ifc")

# Making compound
compound = TopoDS_Compound()
builder = BRep_Builder()
builder.MakeCompound(compound)

# Display the geometrical contents of the file using Python OpenCascade
products = ifc_file.by_type("IfcProduct")
for product in products:
    # Adding a element to compund maked
    if product.is_a("IfcOpeningElement"): continue
    if product.Representation:
        shape = ifcopenshell.geom.create_shape(settings, product).geometry
        display_shape = ifcopenshell.geom.utils.display_shape(shape)
        builder.Add(compound,shape)
        if product.is_a("IfcPlate"):
            # Plates are the transparent parts of the window assembly
            # in the IfcOpenHouse model
            ifcopenshell.geom.utils.set_shape_transparency(display_shape, 0.8)
        if product.is_a("IfcColumn"):
            # Plates are the transparent parts of the window assembly
            # in the IfcOpenHouse model
            print("Trovata Column **************************************************")

# Saving compound
breptools_Write(compound,"scuola.ifc.brep")

# Wait for user input and erase the display
raw_input()
occ_display.EraseAll()

# Enter the main loop so that the user can navigate
ifcopenshell.geom.utils.main_loop()
