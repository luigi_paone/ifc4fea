# -*- coding: utf-8 -*-

import EXAStruTemplate as est

# Setting new instance of section with 
# id = 1 and name = "First Section"
section = est.RCTemplRectEC2(1,"First Section")

section.setDimH(500.0)
section.setDimW(300.0)

# 'MB' means medium bottom area
# 'MT' means medium top area
section.addSteelArea('MB',40.0,600.0)
section.addSteelArea('MT',40.0,600.0)

section.setMaterials('C32/40','B450C')

# Adding Steel Area
section.addTensionPoint2d(N = 500.0, M = 191.2)
section.addTensionPoint2d(N = 0.0,   M = 104.0)

# Building a set points of interaction diagramm
section.interactionDomainBuild2d(nbPoints = 100)

# Adding new Area
section.addSteelArea('MB',40.0,600.0)
section.addSteelArea('MT',40.0,600.0)

# Building a new points set of interaction diagramm
section.interactionDomainBuild2d(nbPoints = 100)

section.interactionDomainPlot2d()

print(section)