# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 18:02:25 2019

@author: lpaone
"""
import math
import numpy as np

import EXAGeometry as eg

class Code(object):

    def __init__(self, codeStr = 'EC2'):   
        if isinstance(codeStr,str):
            self.__byCode = codeStr
        else:
            raise Exception("Only one str argument !!!")
      
    def setCodeStr(self, codeStr):
        if isinstance(codeStr,str):
            self.__byCode = codeStr
        else:
            raise Exception("Only one str argument !!!")
        
    def codeStr(self,):
        return self.__byCode
        
    def __str__(self):
        dispstr = "Code Object: \n"
        dispstr = dispstr + "--------------- \n"
        dispstr = dispstr + " Code str: "  +str(self.__byCode) + "\n"       
        return dispstr
        
class Material(object):

    def __init__(self, ids = -1, types = -1, descr = 'generic'):      
        self.__ids = ids
        self.__types = types
        self.__descr = descr
        self.__byCode = ''
        self.__catstr = ''        
        
    def setByCode(self, codeObj, catstr = ''):
        if isinstance(codeObj,Code):
            self.__byCode = codeObj.codeStr()
        else:
            raise Exception("Only one [Code] argument !!!")
        
    def codeStr(self):
        return self.__byCode
        
    def __str__(self):
        dispstr = "Material Object: \n"
        dispstr = dispstr + "--------------- \n"
        dispstr = dispstr + "  id: "  +str(self.__ids) + "\n"
        dispstr = dispstr + "type: "  +str(self.__types) + "\n"
        dispstr = dispstr + "name: "  +str(self.__descr) + "\n"        
        dispstr = dispstr + "code: "  +str(self.__byCode) + "\n"                
        return dispstr
               
class Concrete(Material):

    def __init__(self, *args):      

        self.__fck  = 0
        self.__fctm = 0
        self.__fcm = 0
        self.__ec2 = 0              
        self.__ecu = 0                          
        self.__lambda = 0
        self.__eta = 0        
        
        if len(args) == 0:
            Material.__init__(self, -1, 1, 'concrete')
        elif len(args) == 1:
            if isinstance(args[0],str):
                Material.__init__(self, -1, 1, args[0])
            else:
                raise Exception("With one arg only str type !!!")                
        else:
            raise Exception("Only one or 0 argument !!!")
    
    def set_fck(self, fck):
        self.__fck = fck
        
    def set_fctm(self, fctm):
        self.__fctm = fctm

    def set_fcm(self, fcm):
        self.__fcm = fcm

    def set_ec2(self, ec2):
        self.__ec2 = ec2

    def set_ecu(self, ecu):
        self.__ecu = ecu

    def set_lambda(self, lm):
        self.__lambda = lm 
        
    def get_fck(self):
        return self.__fck
        
    def get_fctm(self):
        return self.__fctm

    def get_fcm(self):
        return self.__fcm

    def get_ec2(self):
        return self.__ec2

    def get_ecu(self):
        return self.__ecu

    def get_lambda(self):
        return self.__lambda         
        
    def setByCode(self, codeObj, catstr = ''):
        
        super(Concrete,self).setByCode(codeObj,catstr)
               
        if super(Concrete,self).codeStr() == 'EC2' :
            if   catstr == 'C12/15':
                self.__fck  = 12
            elif catstr == 'C16/20':
                self.__fck  = 16
            elif catstr == 'C20/25':
                self.__fck  = 20
            elif catstr == 'C25/30':
                self.__fck  = 25
            elif catstr == 'C30/37':
                self.__fck  = 30
            elif catstr == 'C32/40':
                self.__fck  = 32                
            elif catstr == 'C35/45':
                self.__fck  = 35
            elif catstr == 'C40/50':
                self.__fck  = 40
            elif catstr == 'C45/55':
                self.__fck  = 45
            elif catstr == 'C50/60':
                self.__fck  = 50
            elif catstr == 'C55/67':
                self.__fck  = 55
            elif catstr == 'C60/75':
                self.__fck  = 60  
            elif catstr == 'C70/85':
                self.__fck  = 70  
            elif catstr == 'C80/95':
                self.__fck  = 80  
            elif catstr == 'C90/105':
                self.__fck  = 90                  
            else:
                raise Exception("Catalogue unknown for catstr %s !!!"% catstr)
            
            fck = self.__fck
            self.__fcm = fck + 8    
            if fck>=12.0 and fck<50.0:
                self.__fctm = 0.3*(fck**(2/3))
                self.__ec2 = 0.002              
                self.__ecu = 0.0035                          
                # self.__lambda = 0.8c
                self.__lambda = 0.8
                self.__eta = 1.0
            elif fck>=50.0 & fck<=90.0:
                self.__fctm = 2.12 * math.log(1+self.__fcm/10)
                self.__ec2 = (2.0 + 0.085 * (fck-50.0)**0.53)/1000
                self.__ecu = (2.6 + 35*(((90.0-fck)/100)**4))/1000            
                self.__lambda = 0.8-(fck-50.0)/400.0
                self.__eta = 1.0 + (fck-50.0)/200.0
            else:
                raise Exception("fck must be >=12.0 and <=90.0 !!!")
        else:
            raise Exception("Code unknown !!!")
            
    def __str__(self):
        dispstr = super(Concrete,self).__str__()
        dispstr = dispstr + "Data embedded-->\n"
        dispstr = dispstr + "   fck = " + str(self.__fck)+ "\n"
        dispstr = dispstr + "  fctm = " + str(self.__fctm)+ "\n"
        dispstr = dispstr + "   fcm = " + str(self.__fcm)+ "\n"
        dispstr = dispstr + "   ec2 = " + str(self.__ec2)+ "\n"
        dispstr = dispstr + "   ecu = " + str(self.__ecu)+ "\n"
        dispstr = dispstr + "lambda = " + str(self.__lambda)+ "\n"        
        dispstr = dispstr + "   eta = " + str(self.__eta)+ "\n"         
        return dispstr.replace("\n","\n  | ")
        
class ConcreteSteel(Material):

    def __init__(self, *args): 
        self.__fsy  = 0
        self.__Es = 0
        self.__esy = 0
        self.__esu = 0        
        if len(args) == 0:
            Material.__init__(self, -1, 2, 'concrete steel')
        elif len(args) == 1:
            if isinstance(args[0],str):
                Material.__init__(self, -1, 2, args[0])
            else:
                raise Exception("With one arg only str type !!!")
        else:
            raise Exception("Only one or 0 argument !!!")

    def set_fsy(self, fsy):
        self.__fsy = fsy
        
    def set_Es(self, Es):
        self.__Es = Es

    def set_esy(self, esy):
        self.__esy = esy

    def set_esu(self, esu):
        self.__esu = esu
        
    def get_fsy(self):
        return self.__fsy
        
    def get_Es(self):
        return self.__Es

    def get_esy(self):
        return self.__esy

    def get_esu(self):
        return self.__esu       
        
    def setByCode(self, codeObj, catstr = ''):
        super(ConcreteSteel,self).setByCode(codeObj,catstr)
            
        if super(ConcreteSteel,self).codeStr() == 'EC2' :
            self.__Es = 200000.0
            if   len(catstr)==5 and catstr[0] == 'B':
                if   catstr[4]=='A':
                    self.__esu = 0.0025
                elif catstr[4]=='B':
                    self.__esu = 0.0050                    
                elif catstr[4]=='C':
                    self.__esu = 0.0075
                else:
                    raise Exception("Catalogue unknown for grade <%s> !!!"% catstr[4])                    
                    
                try:
                    # catstr[1:4] fetch char from 1 to 4-1 = 3 ???
                    self.__fsy = float(catstr[1:4])
                except:
                    self.__fsy = 0
                    raise Exception("Yelding stress for catstr <%s> error !!!"% catstr[1:3])                    
            else:
                raise Exception("Catalogue unknown for catstr <%s>. !!!"% catstr)
            self.__esy = self.__fsy/self.__Es          
        else:
            raise Exception("Code unknown !!!")
            
    def __str__(self):
        dispstr = super(ConcreteSteel,self).__str__()
        dispstr = dispstr + "Data embedded-->\n"
        dispstr = dispstr + "fsy = " + str(self.__fsy)+ "\n"
        dispstr = dispstr + " Es = " + str(self.__Es)+ "\n"
        dispstr = dispstr + "esy = " + str(self.__esy)+ "\n"
        dispstr = dispstr + "esu = " + str(self.__esu)+ "\n"           
        return dispstr.replace("\n","\n  | ")
        
class StructSectionItem(object):
    
    def __init__(self, *args):
        if len(args) == 0:
            raise Exception("Only one or >= 2 argument !!!")
        elif len(args) == 2:
            if isinstance(args[0],eg.Shape) and isinstance(args[1],Material):
                self.shape = args[0]
                self.material = args[1]
            else:
                raise Exception("With 2 args type must be Shape and Material !!!")                                
        else:
            raise Exception("Wrong arguments !!!")
            
    def getShape(self):
        return self.shape
        
    def getOrigin(self):
        return self.shape.getOrigin()
        
    def getOriginX(self):
        return self.shape.getOrigin().x

    def getOriginY(self):
        return self.shape.getOrigin().y
        
    def setOriginX(self,x):
        p = self.shape.getOrigin()
        p.x = x
        self.shape.setOrigin(p)

    def setOriginY(self,y):
        p = self.shape.getOrigin()
        p.y = y
        self.shape.setOrigin(p)
        
    def getArea(self):
        return self.shape.getArea()        

    def getMaterial(self):
        return self.material
        
    def __str__(self):
        dispstr =           "StructSectionItem Object: \n"
        dispstr = dispstr + "--------------------  \n"        
        dispstr = dispstr + "Shape embedded-->\n"
        dispstr = dispstr + self.shape.__str__().replace("\n","\n  | ")
        dispstr = dispstr + "Material embedded-->\n"
        dispstr = dispstr + self.material.__str__().replace("\n","\n  | ")
        return dispstr
        
class ConcreteSection(object): 
    
    def __init__(self, *args):
        self.__concreteItem = None
        self.__steelItems = []
        self.__code = Code("EC2")
        
        cls_material = Concrete('EC2-C20/25 [default]')
        cls_material.setByCode(self.__code,"C20/25")
        self.__concreteMaterial = cls_material
        
        steel_material = ConcreteSteel('EC2-B450C [default]')
        steel_material.setByCode(self.__code,'B450C')
        self.__steelMaterial = steel_material
        
        self.__steel_safetyFactorForULS = 1.15
        self.__cls_safetyFactorForULS = 1.50
        
        self.__useSectionMaterials = True
        
        if len(args) == 2:
            if isinstance(args[0],int) and isinstance(args[1],str):
                self.__ids = args[0]
                self.__descr = args[1]
            else:
                raise Exception("With 2 args you need submit int and str type !!!")                   
        elif len(args) == 0:
            self.__ids = 0
            self.__descr = ""
        else:
            raise Exception("Wrong arguments !!!")
            
    def setCode(self, c ):
        if isinstance(c,Code):
            self.__code = c
        else:
            raise Exception("Argument must have [Code] object !!!")
            
    def getCode(self):
        return self.__code        
            
    def setStructConcrItem(self, strusecitem ):
        if isinstance(strusecitem,StructSectionItem):
            if isinstance(strusecitem.getShape(),eg.Shape) and isinstance(strusecitem.getMaterial(),Concrete):
                self.__concreteItem = strusecitem
            else:
                raise Exception("Argument must have [Shape] and [Concrete] as material assigned !!!") 
        else:
            raise Exception("Argument must be [structSectionItem] !!!")            
            
    def setStructSteelItems(self, strusecitems ):
        if isinstance(strusecitems,list):
            for item in strusecitems:
                if not isinstance(item,StructSectionItem):
                    raise Exception("Only list of [structSectionItem] !!!")
                if not (isinstance(item.getShape(),eg.Shape) and isinstance(item.getMaterial(),ConcreteSteel)):
                    raise Exception("List item must have [Shape] and [Steel] as material assigned !!!")
            self.__steelItems = strusecitems
        else:
            raise Exception("Argument must be list of [structSectionItem] !!!")
    
    def getStructConcretelItem(self):
        return self.__concreteItem
        
    def getStructSteelItems(self):
        return self.__steelItems
            
    def findLowSteelItem(self):
        vals = []
        for item in self.__steelItems:
            vals.append(item.getShape().getOrigin().y)
        return self.__steelItems[vals.index(min(vals))]
        
        
    def findHitSteelItem(self):
        vals = []
        for item in self.__steelItems:
            vals.append(item.getShape().getOrigin().y)
        return self.__steelItems[vals.index(max(vals))]
        
    def getSteelTopRecover(self):
        top = self.__concreteItem.getShape().vertexMaxInY()[1]
        bot = self.findHitSteelItem().getShape().getOrigin()
        return (top-bot).y
        
    def getSteelBotRecover(self):
        bot = self.__concreteItem.getShape().vertexMinInY()[1]
        top = self.findLowSteelItem().getShape().getOrigin()
        return (top-bot).y
        
    def getSteelMaterial(self):
        return self.__steelMaterial

    def setConcreteMaterial(self,mat):
        if isinstance(mat,Concrete):
            self.__concreteMaterial = mat
        else:
            raise Exception("You need get in Concrete naterial object !!!")            
        
    def setSteelMaterial(self,mat):
        if isinstance(mat,ConcreteSteel):        
            self.__steelMaterial = mat
        else:
            raise Exception("You need get in ConcreteSteel naterial object !!!")                        
        
    def build2dInteractionCompleteDomain(self, nbPoints = 100):
        
        NxMz_section, Fields = self.build2dInteractionDomain(nbPoints)
        
        
        for si in self.__steelItems:
            si.setOriginY(-si.getOriginY())
        
        NxMz_section_flipped, Fields_flipped = self.build2dInteractionDomain(nbPoints)
        
        for nm in NxMz_section_flipped:
            nm[1] = -nm[1]

        for si in self.__steelItems:
            si.setOriginY(-si.getOriginY())
        
        NxMz_section_flipped.reverse()
        Fields_flipped.reverse()
        
        return NxMz_section + NxMz_section_flipped, Fields + Fields_flipped
        
    def build2dInteractionDomain(self, nbPoints = 100):

        if not (self.__code.codeStr() == "EC2"):
            raise Exception("Implemented now only for EC2 code !!!")                  
        
        if not self.__useSectionMaterials:
            raise Exception("Implemented now only for same material on steel !!!")
            
        if not isinstance(self.__concreteItem.getShape(),eg.ShapeRect):
            raise Exception("Implemented now only for rectangular section shape !!!")
        
        H = self.__concreteItem.getShape().vertexMaxInY()[0]-self.__concreteItem.getShape().vertexMinInY()[0]
        c = self.getSteelBotRecover()
        Hs = self.__concreteItem.getShape().vertexMaxInY()[0]-self.__concreteItem.getShape().getShapePoint('G').y
        Hi = self.__concreteItem.getShape().getShapePoint('G').y-self.__concreteItem.getShape().vertexMinInY()[0]
        Hc = (H/self.__concreteMaterial.get_ecu())*self.__concreteMaterial.get_ec2()
        
        e1 = self.__steelMaterial.get_esu()
        e2 = e1 + self.__concreteMaterial.get_ecu()
        ecs = e2/(H-c)*c
        e3 = e2 + ecs + self.__steelMaterial.get_esu()
        e4 = e3 + self.__concreteMaterial.get_ec2()
        
        # print("H = %1.6f - c = %2.6f - e1 = %3.6f - e2 = %4.6f - e3 = %5.6f - e4 = %6.6f - ecs = %7.6f"%(H,c,e1,e2,e3,e4,ecs))
        # print("Hs = %1.6f - Hi = %2.6f"%(Hs,Hi))
        
        B = self.__concreteItem.getShape().w()
        
        # Convenzioni: sforzi normali positivi se di compressione
        #              flessione positiva prodotta da sforzi normali positivi lato +y
        def sigmas(e):
            
            esu = self.__steelMaterial.get_esu()
            fsy = self.__steelMaterial.get_fsy()
            gamma_s = self.__steel_safetyFactorForULS
            Es = self.__steelMaterial.get_Es()
            
            fsd = fsy/gamma_s
            esd = fsd/Es
            
            if (e>=-esd and e<=+esd):
                sigma = -Es*e
                return sigma
                
            elif (e<-esd and e>=-esu):
                sigma = +fsd
                return sigma
                
            elif (e>esd and e<=esu):
                sigma = -fsd
                return sigma
                
            else:
                raise Exception("sigmas(%1.6e) deformation value error !!!"%e)
                                
        def es(s,y,e1,e2,e3,e4,H,c,Hs,Hi,Hc):
            
            if   (s>=0.0 and s<e1):
                # print("CAMPO 1")
                es = +self.__steelMaterial.get_esu()-(s/(H-c))*(y+Hi-c)
                return es, 1.0
            elif (s>=e1 and s<e2):
                # print("CAMPO 2")
                es = -(-self.__steelMaterial.get_esu()+(s/(H-c))*(y+Hi-c))
                return es, 2.0
            elif (s>=e2 and s<e3):
                # print("CAMPO 3")
                s1 = e3 - s + self.__concreteMaterial.get_ecu()
                # es = ((s1+self.__concreteMaterial.get_ecu())/H)*(Hs-y)-self.__concreteMaterial.get_ecu()
                es = ((s1)/H)*(Hs-y)-self.__concreteMaterial.get_ecu()
                return es, 3.0
            elif (s>=e3 and s<=e4):
                # print("CAMPO 4")
                es = -(((e4-s)/Hc)*(y+Hi-Hc)+self.__concreteMaterial.get_ec2())
                return es, 4.0
            else:
                raise Exception("The value of s is <%1.4f> out of range [%2.6f,%3.6f]!!!"%(s,0,e4))
        
        def xis(s,e1,e2,e3,e4,H,c,Hs,Hi,Hc):
            if   (s>=0.0 and s<e1):
                if s==0.0:
                    xi = -float('Inf')
                else:
                    xi = -(((H-c)/s)*(self.__steelMaterial.get_esu()-s))               
            elif (s>=e1 and s<e2):
                xi = -((H-c)/s)*(self.__steelMaterial.get_esu()-s)                           
            elif (s>=e2 and s<e3):
                s1 = e3 - s + self.__concreteMaterial.get_ecu()
                xi = (self.__concreteMaterial.get_ecu()/s1)*H
            elif (s>=e3 and s<=e4):
                if s==e4:
                    xi = +float('Inf')
                else:                                        
                    xi = ((s-e3)/(e4-s))*Hc+H
            else:
                raise Exception("The value of s is <%1.4f> out of range [%2.6f,%3.6f]!!!"%(s,0,e4))
                
            return xi           

        def Ncs(s,e1,e2,e3,e4,H,c,Hs,Hi,Hc):
            lamda = self.__concreteMaterial.get_lambda()  
            xi = xis(s,e1,e2,e3,e4,H,c,Hs,Hi,Hc)
            if (s>=0.0 and s<e1):
                Ncs = 0.0
                ycs = 0.0
                return Ncs, ycs
                
            elif (s>=e1 and s<e2):
                Ncs = lamda*(self.__concreteMaterial.get_fck()/self.__cls_safetyFactorForULS)*xi
                ycs = Hs-lamda*xi/2
                return Ncs, ycs
                
            elif (s>=e2 and s<e3):
                Ncs = lamda*(self.__concreteMaterial.get_fck()/self.__cls_safetyFactorForULS)*xi
                ycs = Hs-lamda*xi/2
                return Ncs, ycs
                
            elif (s>=e3 and s<=e4):
                if lamda*xi <= H:
                    Ncs = (self.__concreteMaterial.get_fck()/self.__cls_safetyFactorForULS)*lamda*xi        
                    ycs = Hs-lamda*xi/2
                else:
                    Ncs = (self.__concreteMaterial.get_fck()/self.__cls_safetyFactorForULS)*H
                    ycs = Hs-H/2                
                return Ncs, ycs

            else:
                raise Exception("The value of s is <%1.4f> out of range [%2.6f,%3.6f]!!!"%(s,0,e4))     
             
        linearSpace = np.linspace(0,e4,nbPoints)
        
        NxMz_section = []
        ArrayField = []
        
        for i_s in linearSpace:
            N_steel = 0.0
            M_steel = 0.0
            for si in self.__steelItems:
                y_steel = si.getOrigin().y
                Ai = si.getArea()
                e, field = es(i_s,y_steel,e1,e2,e3,e4,H,c,Hs,Hi,Hc)
                N_steel_si = sigmas(e)*Ai
                M_steel_si = N_steel_si*y_steel
                N_steel = N_steel + N_steel_si
                M_steel = M_steel + M_steel_si
                               
            N_concrete = Ncs(i_s,e1,e2,e3,e4,H,c,Hs,Hi,Hc)[0]*B
            y_concrete = Ncs(i_s,e1,e2,e3,e4,H,c,Hs,Hi,Hc)[1]
         
            M_concrete = N_concrete * y_concrete      
            
            NxMz_section.append([N_concrete + N_steel,M_concrete + M_steel])
            ArrayField.append(field)
            
        return NxMz_section, ArrayField
        
    def __str__(self):
        dispstr =           "ConcreteSection Object: \n"
        dispstr = dispstr + "-----------------------  \n"        
        dispstr = dispstr + "  Ids = " + str(self.__ids) + "\n"
        dispstr = dispstr + "Descr = " + str(self.__descr) + "\n"
        dispstr = dispstr + "Steel safety factor for ULS    = " + str(self.__steel_safetyFactorForULS) + "\n"
        dispstr = dispstr + "Concrete safety factor for ULS = " + str(self.__cls_safetyFactorForULS) + "\n"         
        dispstr = dispstr + "---------------------\n"
        dispstr = dispstr + "Materials embedded-->\n"      
        dispstr = dispstr + "---------------------\n"                 
        dispstr = dispstr + str(self.__concreteMaterial).replace("\n","\n  | ")
        dispstr = dispstr + str(self.__steelMaterial).replace("\n","\n  | ")       
        dispstr = dispstr + "-----------------\n"                 
        dispstr = dispstr + "Items embedded-->\n"
        dispstr = dispstr + "-----------------\n"
        if not self.__concreteItem == None:        
            dispstr = dispstr + str(self.__concreteItem).replace("\n","\n  | ")
        else:
            dispstr = dispstr + "Steel without concrete :-)\n"
            
        if not self.__steelItems == None:
            for i in self.__steelItems:
                dispstr = dispstr + str(i).replace("\n","\n  | ")
        else:
            dispstr = dispstr + "Without steel :-)"
        return dispstr            
