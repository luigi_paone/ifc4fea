# -*- coding: utf-8 -*-

import EXAGeometry as eg
import EXAStructural as es

import numpy as np
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt

bar = eg.ShapeCircle(300)
print(bar)

area_shape = eg.ShapeArea(1000)
print(area_shape)

# ******************* CONCRETE SECTION first method

# Setting working code
code_EC2 = es.Code("EC2")
print(code_EC2)

# Setting concrete material 
cls_material = es.Concrete('EC2_C20/25')
cls_material.setByCode(code_EC2,"C20/25")
print(cls_material)

# Setting concrete shape 
rect_shape = eg.ShapeRect(300,500)
print(rect_shape)

# Test iterate over vertex coords
print("Max in x is %1.f"%rect_shape.vertexMaxInX()[0])
print("Max in y is %1.f"%rect_shape.vertexMaxInY()[0])
print("Min in x is %1.f"%rect_shape.vertexMinInX()[0])
print("Min in y is %1.f"%rect_shape.vertexMinInY()[0])

rectangularSection = es.StructSectionItem(rect_shape,cls_material)
print(rectangularSection)

# Setting steel material 
steel_material = es.ConcreteSteel('EC2_450C')
steel_material.setByCode(code_EC2,'B450C')
print(steel_material)

# Testing special vertex
TL = rectangularSection.getShape().getShapePoint('TL')
print('TL **************')
print(TL)
TR = rectangularSection.getShape().getShapePoint('TR')
print('TR **************')
print(TR)
BL = rectangularSection.getShape().getShapePoint('BL')
print('BL **************')
print(BL)
BR = rectangularSection.getShape().getShapePoint('BR')
print('BR **************')
print(BR)
MB = rectangularSection.getShape().getShapePoint('MB')
print('MB **************')
print(MB)
MT = rectangularSection.getShape().getShapePoint('MT')
print('MT **************')
print(MT)
G = rectangularSection.getShape().getShapePoint('G')
print('G **************')
print(G)

# Setting steel shape
area_shape_1 = eg.ShapeArea(200)
area_shape_1.setOrigin(MB+eg.Point2d(0,50))

steel_1 = es.StructSectionItem(area_shape_1,steel_material)
print(steel_1)

area_shape_2 = eg.ShapeArea(200)
area_shape_2.setOrigin(MT+eg.Point2d(0,-50))

steel_2 = es.StructSectionItem(area_shape_2,steel_material)
print(steel_2)

# TODO: __str__ for ConcreteSection
myfirstsection = es.ConcreteSection(1,'300x500 EC2')
myfirstsection.setStructConcrItem(rectangularSection)
myfirstsection.setStructSteelItems([steel_1,steel_2])

print(myfirstsection)

print("Min steel items !!!")
print(myfirstsection.findLowSteelItem())
print("Max steel items !!!")
print(myfirstsection.findHitSteelItem())
print("Steel top recover !!!")
print(myfirstsection.getSteelTopRecover())
print("Steel bot recover !!!")
print(myfirstsection.getSteelTopRecover())

NxMz, Fields = myfirstsection.build2dInteractionDomain()

vertices = NxMz[0:(len(NxMz))]

codes = [Path.MOVETO] + [Path.LINETO]*(len(vertices)-1) # + [Path.CLOSEPOLY]
# codes = [Path.MOVETO] + [Path.LINETO]*(len(vertices)-1)
vertices = np.array(vertices, float)

path = Path(vertices, codes)

# pathpatch = PathPatch(path, facecolor='LightGray', edgecolor='black')
pathpatch = PathPatch(path, facecolor='None', edgecolor='black')

fig, ax = plt.subplots()
ax.add_patch(pathpatch)
ax.set_title('A compound path')

x, y = zip(*path.vertices)
ax.scatter(x, y, marker='o',)

ax.autoscale_view()

plt.show()