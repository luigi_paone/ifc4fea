import numpy as np
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

# def interactionDomainBasePlot2d(vertices, fields, xLabel = 'Nz [N]', yLabel = 'Mz [Nmm]', titleAddStr = None, tensionPoints = None, scale_Nx = 0.001, scale_Mz = 0.000001):
def interactionDomainBasePlot2d(vertices, fields,**kwargs):    
    
    if "xLabel" in kwargs:
        xLabel = kwargs["xLabel"]
    else:
        xLabel = 'Nz [N]'
        
    if "yLabel" in kwargs:
        yLabel = kwargs["yLabel"]
    else:
        yLabel = 'Mz [Nmm]'
        
    if "titleAddStr" in kwargs:
        titleAddStr = kwargs["titleAddStr"]
    else:
        titleAddStr = None
        
    if "tensionPoints" in kwargs:
        tensionPoints = kwargs["tensionPoints"]
    else:
        tensionPoints = None

    if "scale_Nx" in kwargs:
        scale_Nx = kwargs["scale_Nx"]
    else:
        scale_Nx = 0.001

    if "scale_Mz" in kwargs:
        scale_Mz = kwargs["scale_Mz"]
    else:
        scale_Mz = 0.000001
        
    if isinstance(vertices,list) and len(vertices) is not 0 :
        # Only one diagrammpython
        if isinstance(vertices[0][0],float) and isinstance(vertices[0][1],float):
            verticesList = [vertices]
            fieldsList = [fields]
        else:
            verticesList = vertices
            fieldsList = fields
    else:
        raise Exception("Vertices must be a list with lenght > 0 !!!")
            
    fig, ax = plt.subplots()

    if tensionPoints is not None:
        tp = np.array(tensionPoints, float)
        ax.scatter(tp[:,0], tp[:,1], marker='+', s=2000 ,c=[0,0,0])
       
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.xticks(rotation='vertical')
    plt.margins(0.0)
    plt.subplots_adjust(bottom=0.25)
    plt.subplots_adjust(left=0.20)  
    
    strTitle = 'Interaction Domain Nx - Mz'
    if titleAddStr is not None:
        plt.subplots_adjust(top=0.87)         
        strTitle = strTitle + '\n' + titleAddStr
        
    ax.set_title(strTitle)
    ax.grid(True)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2e'))
    
    for idx, vertices_i  in enumerate(verticesList):
        
        codes = [Path.MOVETO] + [Path.LINETO]*(len(vertices_i)-1)
        
        vertices_i = np.array(vertices_i, float)
        
        # setting scale
        scale_Nx = 0.001
        vertices_i[:,0]=vertices_i[:,0] * scale_Nx
        scale_Mz = 0.000001
        vertices_i[:,1]=vertices_i[:,1] * scale_Mz
           
        path = Path(vertices_i, codes)
        pathpatch = PathPatch(path, facecolor='None', edgecolor='black')
        ax.add_patch(pathpatch)
        
        x, y = zip(*path.vertices) # ?
       
        # Displayng Domain Different colors
        scatt_colors = []
        for f in fieldsList[idx]:
            if f == 1.0:
                scatt_colors.append([1,0,0,0.2])
            elif f == 2.0:
                scatt_colors.append([0,1,0,0.2])
            elif f == 3.0:
                scatt_colors.append([0,0,1,0.2])        
            elif f == 4.0:
                scatt_colors.append([1,0,1,0.2])
            else:
                raise Exception("The value of f is <%1.4f> not mapped!!!"%f)
                
        ax.scatter(x, y, marker='o', s=80 ,c=scatt_colors)
           
    ax.autoscale_view()
    
    plt.show()