# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 18:23:33 2019

@author: lpaone
"""
import sys
import math

from OCC.Core.gp import (
                         gp_Pnt, 
                         gp_Lin, 
                         gp_Ax1, 
                         gp_Dir, 
                         gp_Elips, 
                         gp_Ax2, 
                         gp_Ax3,                          
                         gp_Pnt2d,
                         gp_XYZ,
                         gp_Trsf,
                         gp_Vec,                         
                         )
                         
from OCC.Core.BRepBuilderAPI import (BRepBuilderAPI_MakeEdge,
                                     BRepBuilderAPI_MakeVertex,
                                     BRepBuilderAPI_MakeWire,
                                     BRepBuilderAPI_MakeFace,
                                     BRepBuilderAPI_Transform,                                     
                                     # BRepBuilderAPI_GTransform
                                     )

from OCC.Core.GCE2d import GCE2d_MakeLine
from OCC.Core.BRepPrimAPI import BRepPrimAPI_MakeBox #, BRepPrimAPI_MakePrism
from OCC.Core.BRepFeat import BRepFeat_MakePrism
#from OCC.Core.TColgp import TColgp_Array1OfPnt
#from OCC.Core.Geom import Geom_BezierCurve
from OCC.Core.Geom import Geom_Plane
from OCC.Core.BRepLib import breplib_BuildCurves3d
from OCC.Core.BRep import BRep_Tool_Surface
from OCC.Extend.TopologyUtils import TopologyExplorer


from OCC.Display.SimpleGui import init_display
display, start_display, add_menu, add_function_to_menu = init_display()

def perspective(event=None):
    display.SetPerspectiveProjection()
    display.FitAll()

def orthographic(event=None):
    display.SetOrthographicProjection()
    display.FitAll()
    
add_menu('camera projection')    
add_function_to_menu('camera projection', perspective)
add_function_to_menu('camera projection', orthographic)
    
import EXAGeometry as eg

#def exit(event=None):
#    sys.exit()
    
class Visualizer(object):
    
    def __init__(self, *args):
        print("TODO __init__")
        self.__frames = []
        self.__sections = []        
        self.__shapes = []       
        self.__scaleFactorAxis = 0.1
        return
        
    def __str__(self):
        return "TODO __str__"

    def addStruComponent(self, c):
        if isinstance(c,eg.Frame):
            print("addStruComponment FRAME")
            self.__frames.append(c)
        if isinstance(c,eg.Section):
            print("addStruComponment FRAME")
            self.__sections.append(c)            
        else:
            print("TODO others StruComponment")
        return
        
    def __shapeSections__(self):
        sections = self.__sections
        for s in sections:
            if isinstance(s,eg.SectionPoly):
                pl = s.getPolyline2D()
                if pl.isClosed() == False:
                    pl.setClosed()
                nodes = pl.getNodes()
                for i, val in enumerate(nodes[:len(nodes)-1]):
                    pStart = nodes[i]
                    pEnd   = nodes[i+1]
                    edge = BRepBuilderAPI_MakeEdge(gp_Pnt(pStart.x, pStart.y, 0),gp_Pnt(pEnd.x, pEnd.y, 0))            
                    display.DisplayColoredShape(edge.Edge(),'RED', update=True)
                
                # TODO: Adding graphical reference
                # edge = BRepBuilderAPI_MakeEdge(gp_Pnt(pStart.x, pStart.y, 0),gp_Pnt(pEnd.x, pEnd.y, 0))                            
                # display.DisplayVector(gp_Vec(2000,0,0),gp_Pnt(0,0,0))
            else:
                print("TODO sections dont allowed !!!")
        
    def __shapeFrames__(self):
        frames = self.__frames
        
        # Structural set is empty
        if len(frames)==0:
            print("Cannot display frames empty")
            return
            
        for f in frames:
            xi = f.nodeI().x
            yi = f.nodeI().y
            zi = f.nodeI().z
            xj = f.nodeJ().x
            yj = f.nodeJ().y
            zj = f.nodeJ().z
            
            scale_factor = self. __scaleFactorAxis * f.lenght()
            
            # Drawing axis
            axis = BRepBuilderAPI_MakeEdge(gp_Pnt(xi, yi, zi),gp_Pnt(xj, yj, zj))
            display.DisplayColoredShape(axis.Edge(),'BLACK',update=True)
            
            # Moving ref to middle axis
            xi = (xi+xj)/2.
            yi = (yi+yj)/2.
            zi = (zi+zj)/2.
            
            # Drawing local axis
            xDir = scale_factor * f.getXLocalAxis()
            xAxis = BRepBuilderAPI_MakeEdge(gp_Pnt(xi, yi, zi),gp_Pnt(xi+xDir.vx, yi+xDir.vy, zi+xDir.vz))            
            display.DisplayColoredShape(xAxis.Edge(),'RED', update=True)            
            # display.DisplayMessage(gp_Pnt(xi, yi, zi),"axis")
            
            O = BRepBuilderAPI_MakeVertex(gp_Pnt(xi, yi, zi))
            display.DisplayColoredShape(O.Vertex(),'RED', update=True)
            
            X = BRepBuilderAPI_MakeVertex(gp_Pnt(xi+xDir.vx, yi+xDir.vy, zi+xDir.vz))   
            display.DisplayColoredShape(X.Vertex(),'RED', update=True)            
            
            yDir = scale_factor * f.getYLocalAxis()
            yAxis = BRepBuilderAPI_MakeEdge(gp_Pnt(xi, yi, zi),gp_Pnt(xi+yDir.vx, yi+yDir.vy, zi+yDir.vz))            
            display.DisplayColoredShape(yAxis.Edge(),'GREEN', update=True)
            
            zDir = scale_factor * f.getZLocalAxis() 
            zAxis = BRepBuilderAPI_MakeEdge(gp_Pnt(xi, yi, zi),gp_Pnt(xi+zDir.vx, yi+zDir.vy, zi+zDir.vz))            
            display.DisplayColoredShape(zAxis.Edge(),'BLUE', update=True)            
            
            # B = BRepPrimAPI_MakeBox(gp_Pnt(0, 0, 0),1000,1000,1000).Shape()
            # display.DisplayShape(B,color='RED',transparency=0.9,update=True)
            
        return
        
    def display(self):
        print("TODO display")
        self.__shapeFrames__()
        self.__shapeSections__()
        start_display()
        # display.FitAll()
        return

def test_axis(event=None):
    p0 = gp_Pnt(0., 0., 0.)
    p1 = gp_Pnt(50., 0., 0.)
    p2 = gp_Pnt(0., 50., 0.)
    p3 = gp_Pnt(0., 0., 50.)
    edgeX = BRepBuilderAPI_MakeEdge(p0,p1)
    edgeY = BRepBuilderAPI_MakeEdge(p0,p2)
    edgeZ = BRepBuilderAPI_MakeEdge(p0,p3)   
    
    display.EraseAll()
    display.DisplayColoredShape(edgeX.Edge(), 'RED',update=True)
    display.DisplayColoredShape(edgeY.Edge(), 'GREEN',update=True)
    display.DisplayColoredShape(edgeZ.Edge(), 'BLUE',update=True)    
    display.FitAll()    
    start_display()
    
def test_sec(event=None):
    p1 = gp_Pnt(0., 0., 0.)
    p2 = gp_Pnt(300., 0., 0.)
    p3 = gp_Pnt(300., 500., 0.)
    p4 = gp_Pnt(0., 500., 0.)
    edge1 = BRepBuilderAPI_MakeEdge(p1,p2)
    edge2 = BRepBuilderAPI_MakeEdge(p2,p3)
    edge3 = BRepBuilderAPI_MakeEdge(p3,p4)
    edge4 = BRepBuilderAPI_MakeEdge(p4,p1)
    
    display.EraseAll()
    display.DisplayColoredShape(edge1.Edge(), 'BLUE',update=True)
    display.DisplayColoredShape(edge2.Edge(), 'RED',update=True)
    display.DisplayColoredShape(edge3.Edge(), 'YELLOW',update=True)
    display.DisplayColoredShape(edge4.Edge(), 'GREEN',update=True)    
    display.FitAll()
    start_display()
    
def test_vector_sec(event=None):
    p1 = gp_Pnt(0., 0., 0.)
    p2 = gp_Pnt(300., 0., 0.)
    p3 = gp_Pnt(300., 500., 0.)
    p4 = gp_Pnt(0., 500., 0.)
    edge1 = BRepBuilderAPI_MakeEdge(p1,p2)
    edge2 = BRepBuilderAPI_MakeEdge(p2,p3)
    edge3 = BRepBuilderAPI_MakeEdge(p3,p4)
    edge4 = BRepBuilderAPI_MakeEdge(p4,p1)
    
    display.EraseAll()
    display.DisplayColoredShape([edge1.Edge(),edge2.Edge(),edge3.Edge(),edge4.Edge()], 'BLUE',update=True)
    display.FitAll()
    start_display()
    
def test_transform(event=None):
    p1 = gp_Pnt(-150., -250., 0.)
    p2 = gp_Pnt(+150., -250., 0.)
    p3 = gp_Pnt(+150., +250., 0.)
    p4 = gp_Pnt(-150., +250., 0.)
    edge1 = BRepBuilderAPI_MakeEdge(p1,p2)
    edge2 = BRepBuilderAPI_MakeEdge(p2,p3)
    edge3 = BRepBuilderAPI_MakeEdge(p3,p4)
    edge4 = BRepBuilderAPI_MakeEdge(p4,p1)
    
    # system_0_rh = gp_Ax2(gp_Pnt(0.,0.,0.),gp_Dir(0.,0.,1.),gp_Dir(0.,1.,0.))
    system_0_rh = gp_Ax2()
    system_1_rh = gp_Ax2(gp_Pnt(0.,0.,0.),gp_Dir(0.,-0.707,0.707),gp_Dir(1.,0.,0.))
    
    system_0 = gp_Ax3(system_0_rh)
    system_1 = gp_Ax3(system_1_rh)
    transformation = gp_Trsf()
    
    transformation.SetTransformation(system_0,system_1)
     
    
    edge1t = BRepBuilderAPI_Transform(edge1.Edge(),transformation,True)
    edge2t = BRepBuilderAPI_Transform(edge2.Edge(),transformation,True)
    edge3t = BRepBuilderAPI_Transform(edge3.Edge(),transformation,True) 
    edge4t = BRepBuilderAPI_Transform(edge4.Edge(),transformation,True)
    
    
    display.EraseAll()
    display.DisplayColoredShape(edge1.Edge(), 'BLUE',update=True)
    display.DisplayColoredShape(edge2.Edge(), 'RED',update=True)
    display.DisplayColoredShape(edge3.Edge(), 'YELLOW',update=True)
    display.DisplayColoredShape(edge4.Edge(), 'GREEN',update=True) 
    
    display.DisplayColoredShape(edge1t.Shape(), 'BLUE',update=True)
    display.DisplayColoredShape(edge2t.Shape(), 'RED',update=True)
    display.DisplayColoredShape(edge3t.Shape(), 'YELLOW',update=True)
    display.DisplayColoredShape(edge4t.Shape(), 'GREEN',update=True)     
    display.FitAll()
    start_display()
    
def test_extrusion(event=None):
# Make a box
    Box = BRepPrimAPI_MakeBox(400., 250., 300.)
    S = Box.Shape()

    # Choose the first Face of the box
    F = next(TopologyExplorer(S).faces())
    surf = BRep_Tool_Surface(F)

    #  Make a plane from this face
    Pln = Geom_Plane.DownCast(surf)
   
    # Get the normal of this plane. This will be the direction of extrusion.
    D = Pln.Axis().Direction()

    # Inverse normal
    D.Reverse()
    
    MW = BRepBuilderAPI_MakeWire()
    p1 = gp_Pnt2d(200., -100.)
    p2 = gp_Pnt2d(100., -100.)
    aline = GCE2d_MakeLine(p1, p2).Value()
    Edge1 = BRepBuilderAPI_MakeEdge(aline, surf, 0., p1.Distance(p2))
    MW.Add(Edge1.Edge())
    p1 = p2
    p2 = gp_Pnt2d(100., -200.)
    aline = GCE2d_MakeLine(p1, p2).Value()
    Edge2 = BRepBuilderAPI_MakeEdge(aline, surf, 0., p1.Distance(p2))
    MW.Add(Edge2.Edge())
    p1 = p2
    p2 = gp_Pnt2d(200., -200.)
    aline = GCE2d_MakeLine(p1, p2).Value()
    Edge3 = BRepBuilderAPI_MakeEdge(aline, surf, 0., p1.Distance(p2))
    MW.Add(Edge3.Edge())
    p1 = p2
    p2 = gp_Pnt2d(200., -100.)
    aline = GCE2d_MakeLine(p1, p2).Value()
    Edge4 = BRepBuilderAPI_MakeEdge(aline, surf, 0., p1.Distance(p2))
    MW.Add(Edge4.Edge())
    
    #  Build Face from Wire. NB: a face is required to generate a solid.
    MKF = BRepBuilderAPI_MakeFace()
    MKF.Init(surf, False, 1e-6)
    MKF.Add(MW.Wire())
    FP = MKF.Face()
    breplib_BuildCurves3d(FP)

    MKP = BRepFeat_MakePrism(S, FP, F, D, False, True)
    MKP.Perform(200.)
    # TODO MKP completes, seeing a split operation but no extrusion
    assert MKP.IsDone()
    res1 = MKP.Shape()

    display.EraseAll()
    display.DisplayColoredShape(res1, 'BLUE',update=True)
    display.FitAll()    
    start_display()
