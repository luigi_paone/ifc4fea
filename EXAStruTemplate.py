# -*- coding: utf-8 -*-
"""
Created on Thu Avr 27 09:28:00 2019

@author: lpaone
"""
import EXAGeometry as eg
import EXAStructural as es
import EXAPlot2D as ep

class RCTemplRectEC2(object):
    
    def __init__(self, *args):
        
        if len(args) == 0:
            self.__section = es.ConcreteSection()

        elif len(args) == 2:
            self.__section = es.ConcreteSection(args[0],args[1])
                       
        else:
            raise Exception("Wrong args !!!")
            
        code = es.Code("EC2")
        self.__section.setCode(code)
            
        cls_material = es.Concrete("Concrete EC2 for template")
        cls_material.setByCode(code,"C32/40")            
        self.__section.setConcreteMaterial(cls_material)
            
        rect_shape = eg.ShapeRect()
        rectangularSection = es.StructSectionItem(rect_shape,cls_material)
        self.__section.setStructConcrItem(rectangularSection)
            
        steel_material = es.ConcreteSteel('Steel EC2 for template')
        steel_material.setByCode(code,'B450C')
        self.__section.setSteelMaterial(steel_material)
        
        # New starting from structural section
        self.__tensionPoints2d = []
        self.__domainPoints2d = [] # ultimate domain
        self.__fieldsPoints2d = [] # ultimate domain field

        # Param for plot2D interaction domain        
        self.__plot2dInteraction_xLabel = 'Nz [N]'
        self.__plot2dInteraction_yLabel = 'Mz [Nmm]'
        self.__plot2dInteraction_titleAddStr = None
        self.__plot2dInteraction_scale_Nx = 0.001 
        self.__plot2dInteraction_scale_Mz = 0.000001        
        
        self.__plot2dInteraction_nbPoints = 100
    
    def setDimH(self, h):
        if isinstance(h,float):
            self.__section.getStructConcretelItem().getShape().setDimH(h)
        else:
            raise Exception("Only one float for h!!!")
    
    def setDimW(self, w):
        if isinstance(w,float):
            self.__section.getStructConcretelItem().getShape().setDimW(w)
        else:
            raise Exception("Only one float for w !!!")
    
    def addSteelArea(self, posStr, dist, area):
        if isinstance(posStr,str) is not True:
            raise Exception("First arg must be a str type !!!")            
        if isinstance(dist,float) is not True:
            raise Exception("Second arg must be a float type !!!")                        
        if isinstance(area,float) is not True:
            raise Exception("Third arg must be a float type !!!")                                    
        if posStr == 'MB':
            shape = self.__section.getStructConcretelItem().getShape()
            MB = shape.getShapePoint('MB')
            steel_area = eg.ShapeArea(area)
            steel_area.setOrigin(MB+eg.Point2d(0,dist))
            steel_item = es.StructSectionItem(steel_area,self.__section.getSteelMaterial())
            self.__section.getStructSteelItems().append(steel_item)
        elif posStr == 'MT':
            shape = self.__section.getStructConcretelItem().getShape()
            MT = shape.getShapePoint('MT')
            steel_area = eg.ShapeArea(area)
            steel_area.setOrigin(MT+eg.Point2d(0,-dist))
            steel_item = es.StructSectionItem(steel_area,self.__section.getSteelMaterial())
            self.__section.getStructSteelItems().append(steel_item)
            
        
    def setMaterials(self, concreteStr, steelStr):
        cls_material = es.Concrete('Concrete EC2 for template')
        cls_material.setByCode(self.__section.getCode(),concreteStr)
        self.__section.setConcreteMaterial(cls_material)

        steel_material = es.ConcreteSteel('Steel EC2 for template')
        steel_material.setByCode(self.__section.getCode(),steelStr)
        self.__section.setSteelMaterial(steel_material)
    
    def addTensionPoint2d(self, N, M):
        self.__tensionPoints2d.append([N,M])

    def tensionPoint2d(self, N, M):
        return self.__tensionPoints2d
        
    def interactionDomainBuild2d(self, **kwargs):
        if "nbPoints" in kwargs:
            _nbPoints = kwargs["nbPoints"]
        else:
            _nbPoints = self.__plot2dInteraction_nbPoints
            
        NxMz, fields = self.__section.build2dInteractionCompleteDomain(_nbPoints)
        self.__domainPoints2d.append(NxMz)
        self.__fieldsPoints2d.append(fields)
        
    def interactionDomainPlot2d(self,**kwargs):
        if "xLabel" in kwargs:
            _xLabel = kwargs["xLabel"]
        else:
            _xLabel = self.__plot2dInteraction_xLabel
            
        if "yLabel" in kwargs:
            _yLabel = kwargs["yLabel"]
        else:
            _yLabel = self.__plot2dInteraction_yLabel
            
        if "titleAddStr" in kwargs:
            _titleAddStr = kwargs["titleAddStr"]
        else:
            _titleAddStr = self.__plot2dInteraction_titleAddStr
                
        if "scale_Nx" in kwargs:
            _scale_Nx = kwargs["scale_Nx"]
        else:
            _scale_Nx = self.__plot2dInteraction_scale_Nx
    
        if "scale_Mz" in kwargs:
            _scale_Mz = kwargs["scale_Mz"]
        else:
            _scale_Mz = self.__plot2dInteraction_scale_Mz    
    
        ep.interactionDomainBasePlot2d(
        self.__domainPoints2d,
        self.__fieldsPoints2d, 
        xLabel = _xLabel, 
        yLabel = _yLabel, 
        titleAddStr = _titleAddStr, 
        tensionPoints = self.__tensionPoints2d,
        scale_Nx = _scale_Nx,
        scale_Mz = _scale_Mz        
        )
    
    def getSection(self):
        return self.__section
        
    def __str__(self):
        dispstr =           "RCTemplRectEC2 Object: \n"
        dispstr = dispstr + "-----------------------  \n"
        dispstr = dispstr + "Tension Points 2d = " + str(self.__tensionPoints2d) + "\n"        
        dispstr = dispstr + "-----------------\n"                 
        dispstr = dispstr + "Section embedded-->\n"
        dispstr = dispstr + "------------------\n"
        dispstr = dispstr + str(self.__section).replace("\n","\n  | ")
        return dispstr           
        
    